## Environment setup
1) Create a virtual envrionment and activate it (for a review of how to do that, see [this](https://docs.python-guide.org/dev/virtualenvs/)).  This code was tested with Python 3.7.2, but it should work with any version from 3.5-3.7.
2) `pip install -r requirements.txt` to install dependencies.
3) Install [PyTorch](https://pytorch.org/) into your virtual environment.  This step is platform-specific, and so a package isn't specified in requirements.txt.
4) Start Jupyter Lab on your machine with `jupyter lab`
5) Navigate in a web browser to the link that pops up in the terminal after the previous step.  The URL [`http://localhost:8888/lab`](http://localhost:8888/lab) should work as well.